
## Introducere

   Platforma vine in sprijinul studentilor care doresc sa isi stocheze notitele intr-un mod organizat in functie de materiile 
de care sunt interesati.  
   De aplicatie vor beneficia studentii ce detin un cont institutional de tipul ‘@stud.ase.ro’. Acestia vor avea posibilitatea
sa vizualizeze, adauge, editeze si sterge propriile cursuri, iar in cazul in care doresc sa partajeze informatiile obtinute, 
le vor putea impartasi cu alti colegi prin intermediul grupurilor de studiu. 
Daca este necesar, in cadrul notitelor scrise pe platforma, studentii pot adauga atasamente precum imagini, documente, link-uri,
pentru a avea mai multe detalii cu privire la respctivul subiect.

   Aplicatii asemanatoare cu cea realizata de noi sunt StuDoc si EverNote.
	
   StuDoc reprezinta o platforma online dedicata studentilor, in care acestia pot invata si impartasi informatii despre 
diferite teme/subiecte abordate de ei la cursuri/seminarii. 
   Fiecare student, cand isi face un cont, este obligat sa-si specific facultatea, pentru a fi mai usor de gasit de catre 
ceilalti student. 
   Fiecare student poate cauta/adauga cursuri/seminarii/notite in legatura cu o anumita tema. Odata ce face asta, isi asuma ca
lucrarea publicata sa poata fi descarcata de catre oricare alti utilizatori.
   Lucrarile publicate pot fi share-uite, comentate (in caz ca cititorul are anumite intrebari legate de lucrare) sau apreciate
in functie de calitatea lor si ajutorul acordat prin ele. (like/dislike)
   Fiecare utilizator al aplicatiei poate primi punctaj in functie de activitatea desfasurata: incarcarea documentelor, 
primirea comentariilor, primirea aprecierilor la documentele incarcate si vizualizarea documentelor, la randul sau, ca si 
cititor.
	
   EverNote reprezinta o platforma online dedicata atat studentilor cat si angajatilor de companii, in care acestia isi pot 
scrie propriile notite, pe care la randul lor le pot impartasi cu anumite persoane
   Utilizatorii pot vizualiza notitele altor persoane numai daca acestia au fost de acord sa le impartaseasca cu ei.
   Toate notitele pe care utilizatorii le scriu, vor fi organizate intr-o mica agenda.
   Utilizatorii isi pot scrie notitele, nu si incarca de pe o anumita sursa, intr-un editor text.


	

## Interfete aplicatie

![](/Documentatie/Capture.PNG)


## API REST

![](/Documentatie/Capture2.PNG)