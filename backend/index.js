const express = require("express");
const session = require("express-session");
const bodyParser = require("body-parser");
const config = require("./config.json");
const app = express();
const { port, secret } = config;
const cors = require("cors");
const { whitelist } = config.cors;
const router = require("./router");
const { DB } = require("./models");
const fileUpload = require("express-fileupload");
const corsOptions = {
    origin: (origin, callback) => {
        if (whitelist.indexOf(origin) !== -1 || typeof origin === "undefined") {
            callback(null, true);
        } else {
            callback(new Error("Not allowed by CORS"));
        }
    },
    credentials: true
};
app.use(fileUpload({}));
app.use(
    session({
        resave: false,
        secret,
        saveUninitialized: false
    })
);
DB.sync().then(() => {
    app.use(bodyParser.json({ type: "application/*+json" }));
    app.use(cors(corsOptions));
    app.use("/", router);
    app.listen(port, () => console.log(`SPA app listening on port ${port}!`));
});
