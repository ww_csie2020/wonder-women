const { Group, User, GroupUser, CourseGroup, Course } = require("../models");
const group = {
    update: async group => {
        try {
            const result = await Group.update(group, {
                where: { id: group.id, userId: group.userId }
            });
            await GroupUser.destroy({
                where: {
                    groupId: group.id
                }
            });
            await CourseGroup.destroy({
                where: {
                    groupId: group.id
                }
            });
            await Promise.all(
                group.users.map(async userId => {
                    await GroupUser.create({ userId, groupId: group.id });
                })
            );
            await Promise.all(
                group.courses.map(async courseId => {
                    CourseGroup.create({ courseId, groupId: group.id });
                })
            );
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    create: async group => {
        try {
            const result = await Group.create(group);
            if (result) {
                await GroupUser.destroy({
                    where: {
                        groupId: result.id
                    }
                });
                await Promise.all(
                    group.users.map(async userId => {
                        await GroupUser.create({ userId, groupId: result.id });
                    })
                );
                await Promise.all(
                    group.courses.map(async courseId => {
                        CourseGroup.create({ courseId, groupId: group.id });
                    })
                );
            }

            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getAll: async userId => {
        try {
            const groups = await Group.findAll({
                where: { userId },
                include: [
                    {
                        model: User,
                        attributes: ["email", "firstName", "lastName", "id"]
                    },
                    {
                        model: Course,
                        attributes: ["name", "id"]
                    }
                ]
            });
            return groups;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    delete: async (id, userId) => {
        try {
            await GroupUser.destroy({
                where: {
                    groupId: id
                }
            });
            await CourseGroup.destroy({
                where: {
                    groupId: id
                }
            });
            return await Group.destroy({
                where: {
                    id,
                    userId
                }
            });
        } catch (err) {
            throw new Error(err.message);
        }
    }
};

module.exports = group;
