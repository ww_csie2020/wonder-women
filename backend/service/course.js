const { Course, Note, OP, CourseGroup, GroupUser } = require("../models");
const course = {
    update: async course => {
        try {
            const result = await Course.update(course, {
                where: { id: course.id, userId: course.userId }
            });
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    create: async course => {
        try {
            const result = await Course.create(course);
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    getAll: async userId => {
        try {
            const courses = await Course.findAll({
                where: { userId }
            });
            return courses;
        } catch (err) {
            throw new Error(err.message);
        }
    },

    getWithNotes: async userId => {
        try {
            const courses = await Course.findAll({
                where: { userId },
                include: [
                    {
                        model: Note,
                        attributes: ["name", "id"]
                    }
                ]
            });
            return courses;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    
    getSharedWithNotes: async userId => {
        try {
            const groups = await GroupUser.findAll({
                where: {
                    userId
                }
            }).map(e => e.groupId);
            const courses = await CourseGroup.findAll({
                where: {
                    groupId: {
                        [OP.in]: groups
                    }
                }
            }).map(c => c.courseId);

            const result = await Course.findAll({
                where: {
                    id: {
                        [OP.in]: courses
                    }
                },
                include: [
                    {
                        model: Note,
                        attributes: ["name", "id"]
                    }
                ]
            });
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },


    delete: async (id, userId) => {
        try {
            return await Course.destroy({
                where: {
                    id,
                    userId
                }
            });
        } catch (err) {
            throw new Error(err.message);
        }
    }
};

module.exports = course;
