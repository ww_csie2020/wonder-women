const { Note, Course, CourseGroup, OP, Group, NoteUser, User, Attachment } = require("../models");
const fs = require("fs");
const uuid = require("uuid/v4");
const findCourse = async (courseId, userId) => {
    const course = await Course.findOne({
        where: { id: courseId, userId }
    });
    if (!course) {
        throw new Error(`Course ${courseId} does not exist`);
    }
};

const deleteAttachments = async (noteId, attachments) => {
    const existingAttachments = await Attachment.findAll({
        where: {
            noteId
        }
    });
    await Promise.all(
        existingAttachments.map(async attachment => {
            if (attachments.indexOf(attachment.id) === -1) {
                if (attachment.type === "FILE") {
                    await fs.unlink("files/" + attachment.value, () => {});
                }
                await attachment.destroy();
            }
        })
    );
};

createAttachments = async (noteId, attachments, files) => {
    if (files) {
        const filesArr = Array.isArray(files) ? files : [files];
        filesArr.map(file => {
            if (["image/jpeg", "image/gif", "text/csv", "image/png", "application/pdf", "image/svgxml"].indexOf(file.mimetype) !== -1) {
                const fileName = uuid() + file.name;
                attachments.push({
                    type: "FILE",
                    value: fileName
                });
                fs.writeFile("files/" + fileName, file.data, err => {});
            }
        });
    }
    await Promise.all(
        attachments.map(async attachment => {
            if (!attachment.id) {
                await Attachment.create({
                    noteId,
                    value: attachment.value,
                    type: attachment.type
                });
            }
        })
    );
};

const note = {
    update: async (note, courseId, userId, files) => {
        try {
            await findCourse(courseId, userId);
            await deleteAttachments(
                note.id,
                note.attachments.map(a => a.id)
            );
            await createAttachments(note.id, note.attachments, files);
            await NoteUser.destroy({
                where: {
                   noteId: note.id
                }
            });
            await Promise.all(
                note.users.map(async user => {
                    await NoteUser.create({
                        noteId: note.id,
                        userId:user
                    });
                })
            );
            const result = await Note.update(note, {
                where: { id: note.id }
            });
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    create: async (note, courseId, userId, files) => {
        try {
            await findCourse(courseId, userId);
            const result = await Note.create(note);
            await Promise.all(
                note.users.map(async userId => {
                    await NoteUser.create({
                        noteId: result.id,
                        userId
                    });
                })
            );
            
            await createAttachments(result.id, note.attachments, files);
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    },
    delete: async (id, courseId, userId) => {
        try {
            await findCourse(courseId, userId);
            await deleteAttachments(id, []);
            await NoteUser.destroy({
                where: {
                  noteId: id
                }
            });
            return await Note.destroy({
                where: {
                    id
                }
            });
        } catch (err) {
            throw new Error(err.message);
        }
    },
    get: async (id, courseId, userId) => {
        try {
            const course = await Course.findOne({
                where: {
                    id: courseId,
                    userId: userId
                }
            });
            if (!course) {
                //First check if the note is assigned to a user
                const userNote = await NoteUser.findOne({
                    where: {
                        userId,
                        noteId: id
                    }
                });
                if (!userNote) {
                    //If the note is not shared with a user check if it's shared with a group of users through a course group sharing
                    const groups = await Group.findAll({
                        where: {
                            userId
                        },
                        attributes: ["id"]
                    }).map(g => g.id);
                    const courseGroup = await CourseGroup.findOne({
                        where: {
                            courseId,
                            groupId: {
                                [OP.in]: groups
                            }
                        }
                    });
                    if (!courseGroup) {
                        throw new Error("You don't have access to the note");
                    }
                }
            }
            let result = await Note.findOne({
                where: {
                    id,
                    courseId
                },
                include: [Attachment]
            });

            if (result) {
                const users = await NoteUser.findAll({
                    where: {
                        noteId: id
                    }
                });
                result.dataValues.users = users.map(u => u.userId);
            }
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
        
         },
    getShared: async userId => {
        try {
            const notes = await NoteUser.findAll({
                where: {
                    userId
                }
            });
            const result = await Note.findAll({
                where: {
                    id: {
                        [OP.in]: notes.map(n => n.noteId)
                    }
                }
            });
            return result;
        } catch (err) {
            throw new Error(err.message);
        }
    }
};

module.exports = note;
