const DB = require("./../db");
const Sequelize = require("sequelize");
const bcrypt = require("bcrypt");

class User extends Sequelize.Model {
    static generateHash(password) {
        return bcrypt.hash(password, bcrypt.genSaltSync(8));
    }
    validPassword(password) {
        return bcrypt.compare(password, this.password);
    }
}

User.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        lastName: {
            type: Sequelize.STRING
        },
        firstName: {
            type: Sequelize.STRING
        },
        email: {
            type: Sequelize.STRING,
            allowNull: false
        },
        password: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    {
        sequelize: DB,
        modelName: "users"
    }
);

class Group extends Sequelize.Model {}
Group.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    {
        sequelize: DB,
        modelName: "groups"
    }
);

class GroupUser extends Sequelize.Model {}
GroupUser.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        }
    },
    {
        sequelize: DB,
        modelName: "groupUser"
    }
);

class CourseGroup extends Sequelize.Model {}
CourseGroup.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        }
    },
    {
        sequelize: DB,
        modelName: "courseGroup"
    }
);

class NoteUser extends Sequelize.Model {}
NoteUser.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        }
    },
    {
        sequelize: DB,
        modelName: "noteUser"
    }
);
class Course extends Sequelize.Model {}
Course.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        description: {
            type: Sequelize.TEXT
        }
    },
    {
        sequelize: DB,
        modelName: "courses"
    }
);

class Attachment extends Sequelize.Model {}
Attachment.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        type: {
            type: Sequelize.ENUM,
            values: ["FILE", "LINK"],
            allowNull: false
        },
        value: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    {
        sequelize: DB,
        modelName: "attachments"
    }
);

class Note extends Sequelize.Model {}
Note.init(
    {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        content: {
            type: Sequelize.TEXT
        }
    },
    {
        sequelize: DB,
        modelName: "notes"
    }
);

Note.hasMany(Attachment);
Note.belongsTo(Course);

Group.belongsToMany(User, {
    through: GroupUser,
    foreignKey: "groupId"
});
Group.hasMany(Course);

Course.hasMany(Note);
Course.belongsToMany(Group, {
    through: CourseGroup,
    foreignKey: "courseId"
});

User.hasMany(Course, {}, { onDelete: "cascade" });
User.hasMany(Group);

Note.belongsToMany(User, {
    through: NoteUser,
    foreignKey: "noteId"
});

module.exports = {
    DB,
    OP: Sequelize.Op,
    User,
    Note,
    NoteUser,
    GroupUser,
    Group,
    Course,
    CourseGroup,
    Attachment
};
