const noteService = require("../service/note");
const createNote = async (req, res) => {
    try {
        const note = JSON.parse(req.files.data.data.toString());
        const files = req.files.attachments;
        const userId = req.session.userId;
        if (note.name && note.content && note.courseId) {
            const result = await noteService.create(note, note.courseId, userId, files);
            if (result) {
                res.status(201).send({
                    message: "Note created successfully"
                });
            } else {
                res.status(400).send({
                    message: "Error occurred while trying to create the note"
                });
            }
        } else {
            res.status(400).send({
                message: "Invalid note"
            });
        }
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const updateNote = async (req, res) => {
    try {
        const note = JSON.parse(req.files.data.data.toString());
        const files = req.files.attachments;
        note.id = req.params.id;
        const userId = req.session.userId;
        if (note.name && note.id && note.content && note.courseId) {
            const result = await noteService.update(note, note.courseId, userId, files);
            if (result) {
                res.status(201).send({
                    message: "Note updated successfully"
                });
            } else {
                res.status(400).send({
                    message: "Error occurred while trying to update the note"
                });
            }
        } else {
            res.status(400).send({
                message: "Invalid note"
            });
        }
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const getNote = async (req, res) => {
    try {
        const noteId = req.params.id;
        const courseId = req.params.courseId;
        const userId = req.session.userId;
        const note = await noteService.get(noteId, courseId, userId);
        res.status(200).send(note);
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const getSharedNotes = async (req, res) => {
    try {
        const userId = req.session.userId;
        const note = await noteService.getShared(userId);
        res.status(200).send(note);
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const deleteNote = async (req, res) => {
    try {
        const noteId = req.params.id;
        const courseId = req.params.courseId;
        const userId = req.session.userId;
        const note = await noteService.delete(noteId, courseId, userId);
        res.status(200).send();
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

module.exports = {
    createNote,
    updateNote,
    getNote,
    deleteNote,
    getSharedNotes
};
