const getFile = async (req, res) => {
    const fileName = req.params.name;

    res.download("files/" + fileName);
};

module.exports = { getFile };