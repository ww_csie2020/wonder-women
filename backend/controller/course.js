const courseService = require("./../service/course");

const createCourse = async (req, res) => {
    const course = req.body;
    if (course.name) {
        course.userId = req.session.userId;
        const result = await courseService.create(course);
        res.status(201).send({
            message: "Course created successfully"
        });
    } else {
        res.status(400).send({
            message: "Invalid course."
        });
    }
};

const updateCourse = async (req, res) => {
    const course = req.body;
    if (course.name && course.id) {
        course.id = req.params.id;
        course.userId = req.session.userId;
        const result = await courseService.update(course);
        res.status(201).send({
            message: "Course updated successfully"
        });
    } else {
        res.status(400).send({
            message: "Invalid course."
        });
    }
};

const getAllCourses = async (req, res, next) => {
    try {
        const courses = await courseService.getAll(req.session.userId);
        res.status(200).send(courses);
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const getAllCoursesWithNotes = async (req, res, next) => {
    try {
        const courses = await courseService.getWithNotes(req.session.userId);
        res.status(200).send(courses);
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const getSharedCourses = async (req, res, next) => {
    try {
        const courses = await courseService.getSharedWithNotes(
            req.session.userId
        );
        res.status(200).send(courses);
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};


const deleteCourse = async (req, res) => {
    try {
        await courseService.delete(req.params.id, req.session.userId);
        res.status(200).send();
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

module.exports = {
    createCourse,
    getAllCourses,
    deleteCourse,
    updateCourse,
    getAllCoursesWithNotes,
    getSharedCourses
};
