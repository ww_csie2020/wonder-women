const userService = require("./../service/user");

const createUser = async (req, res) => {
    const user = req.body;
    if (user.firstName && user.lastName && user.email && user.password) {
        const result = await userService.create(user);
        if (result) {
            res.status(201).send({
                message: "User created successfully"
            });
        } else {
            res.status(400).send({
                message: "Error occurred while trying to create the user"
            });
        }
    } else {
        res.status(400).send({
            message: "Invalid user"
        });
    }
};

const getAllUsers = async (req, res, next) => {
    try {
        const users = await userService.getAll(req.session.userId);
        res.status(200).send(users);
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const deleteUser = async (req, res) => {
    try {
        await userService.delete(req.params.id);
        res.status(200).send();
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const login = async (req, res) => {
    try {
        const user = req.body;
        const userFound = await userService.login(user.email, user.password);
        if (userFound) {
            req.session.userId = userFound;
        }
        res.status(200).send({
            success: !!userFound,
            userId: userFound
        });
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const logout = async (req, res) => {
    req.session.destroy();
    res.status(200).send({});
};

const checkUserLoginStatus = async (req, res) => {
    try {
        if (req.session.userId) {
            const currentUser = await userService.getById(req.session.userId);
            res.status(200).send(currentUser);
        }
    } catch (err) {}
};

module.exports = {
    createUser,
    getAllUsers,
    deleteUser,
    login,
    checkUserLoginStatus,
    logout
};
