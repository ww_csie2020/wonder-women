const groupService = require("./../service/group");

const createGroup = async (req, res) => {
    const group = req.body;
    if (group.name) {
        group.userId = req.session.userId;
        const result = await groupService.create(group);
        if (result) {
            res.status(201).send({
                message: "Group created successfully"
            });
        } else {
            res.status(400).send({
                message: "Invalid group."
            });
        }
    } else {
        res.status(400).send({
            message: "Invalid group."
        });
    }
};
const updateGroup = async (req, res) => {
    const group = req.body;
    if (group.name) {
        group.userId = req.session.userId;
        group.id = req.params.id;
        const result = await groupService.update(group);
        if (result) {
            res.status(201).send({
                message: "Group updated successfully"
            });
        } else {
            res.status(400).send({
                message: "Invalid group."
            });
        }
    } else {
        res.status(400).send({
            message: "Invalid group."
        });
    }
};

const getAllGroups = async (req, res, next) => {
    try {
        const groups = await groupService.getAll(req.session.userId);
        res.status(200).send(groups);
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

const deleteGroup = async (req, res) => {
    try {
        await groupService.delete(req.params.id, req.session.userId);
        res.status(200).send();
    } catch (err) {
        res.status(500).send({
            message: `Error occurred: ${err.message}`
        });
    }
};

module.exports = { createGroup, getAllGroups, deleteGroup, updateGroup };
