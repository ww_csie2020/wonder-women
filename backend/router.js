const express = require("express");
const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

const { createUser, getAllUsers, deleteUser, login, logout, checkUserLoginStatus } = require("./controller/user");
const { createCourse, getAllCourses, deleteCourse, updateCourse, getAllCoursesWithNotes, getSharedCourses } = require("./controller/course");
const { createGroup, getAllGroups, deleteGroup, updateGroup } = require("./controller/group");
const { createNote, updateNote, getNote, deleteNote, getSharedNotes } = require("./controller/note");
const { getFile } = require("./controller/file");

const router = express.Router();

/**
 * Add login middleware
 */
router.use((req, res, next) => {
    if (req.originalUrl === "/login" || req.originalUrl === "/register") {
        next();
    } else if (req.session.userId) {
        next();
    } else {
        res.status(403).send();
    }
});

router.post("/login", jsonParser, login);
router.post("/register", jsonParser, createUser);
router.post("/logout", logout);

router.get("/users", getAllUsers);
router.get("/user/isLoggedIn", checkUserLoginStatus);
router.delete("/users/:id", deleteUser);

router.get("/courses", jsonParser, getAllCourses);
router.get("/courses/notes", jsonParser, getAllCoursesWithNotes);
router.post("/courses", jsonParser, createCourse);
router.post("/courses/:id", jsonParser, updateCourse);
router.delete("/courses/:id", deleteCourse);
router.get("/courses/shared", jsonParser, getSharedCourses);

router.get("/groups", jsonParser, getAllGroups);
router.post("/groups", jsonParser, createGroup);
router.post("/groups/:id", jsonParser, updateGroup);
router.delete("/groups/:id", deleteGroup);

router.post("/notes", createNote);
router.post("/notes/:id", updateNote);
router.get("/notes/:courseId/:id", jsonParser, getNote);
router.get("/notes/shared", jsonParser, getSharedNotes);
router.delete("/notes/:courseId/:id", deleteNote);

router.get("/file/:name", getFile);

module.exports = router;
