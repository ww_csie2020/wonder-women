import React, { Component } from "react";
import Http from "../../http";
import { toast } from "bulma-toast";
import parse from "html-react-parser";
import marked from "marked";
const { server } = require("../../config.json");
class Login extends Component {
    constructor(props) {
        super(props);
        this.fileInput = React.createRef();
    }
    state = {
        courses: [],
        sharedCourses: [],
        sharedNotes: [],
        selectedNote: {
            name: "",
            content: "",
            users: [],
            attachments: []
        },
        selectedCourse: "",
        users: [],
        attachment: "",
        files: []
    };

    handleChange(field, e) {
        const selectedNote = { ...this.state.selectedNote };
        selectedNote[field] = e.target.value;

        this.setState({ selectedNote });
    }

    save(e) {
        e.preventDefault();
        let dataJson = { ...this.state.selectedNote };
        dataJson.courseId = this.state.selectedCourse;
        if (this.state.selectedCourse !== "") {
            const data = new FormData();
            const json = JSON.stringify(dataJson);
            data.append("data", new Blob([json]), { type: "application/json" });

            for (var fileIndex = 0; fileIndex < this.state.files.length; fileIndex++) {
                data.append("attachments", this.state.files[fileIndex]);
            }

            Http.post("/notes" + (this.state.selectedNote.id ? `/${this.state.selectedNote.id}` : ""), data).then(e => {
                this.loadNotes();
                toast({
                    message: e.message,
                    type: "is-success",
                    position: "center"
                });

                this.newNote();
            });
        } else {
            toast({
                message: "Please first select a course",
                type: "is-danger"
            });
        }
    }

    delete() {
        Http.delete(`/notes/${this.state.selectedNote.courseId}/${this.state.selectedNote.id}`).then(e => {
            this.loadNotes();
            toast({
                message: "Deleted successfully",
                type: "is-success",
                position: "center"
            });
            this.newNote();
        });
    }

    loadNotes() {
        Http.get(`/courses/notes`).then(data => {
            this.setState({ courses: data });
        });
    }

    componentDidMount() {
        this.loadNotes();

        Http.get("/users").then(data => {
            this.setState({ users: data });
        });
        Http.get("/courses/shared").then(data => {
            this.setState({ sharedCourses: data });
        });
        Http.get("/notes/shared").then(data => {
            this.setState({ sharedNotes: data });
        });
    }

    selectNote(noteId, courseId, isShared = false) {
        Http.get(`/notes/${courseId}/${noteId}`).then(data => {
            data.isShared = isShared;
            this.setState({
                selectedNote: data,
                selectedCourse: data.courseId
            });
        });
    }

    selectCourse(courseId) {
        this.setState({ selectedCourse: courseId });
    }

    newNote() {
        this.setState({
            selectedNote: {
                isShared: false,
                name: "",
                content: "",
                users: [],
                attachments: []
            },
            selectedCourse: "",
            files: []
        });
        this.fileInput.current.value = "";
    }

    addAttachment(e) {
        this.setState({ attachment: e.target.value });
    }

    saveAttachment(e) {
        const currentNote = { ...this.state.selectedNote };
        currentNote.attachments.push({ type: "LINK", value: this.state.attachment });
        this.setState({ selectedNote: currentNote, attachment: "" });
    }

    onFileUpload(e) {
        this.setState({ files: e.target.files });
    }

    getAttachment(attachment, index, noDelete = false) {
        if (attachment.type === "LINK") {
            const isRealUrl = attachment.value.startsWith("http://") || attachment.value.startsWith("https://");
            return (
                <span className="tag" key={attachment.id || attachment.value}>
                    {isRealUrl && <a href={attachment.value}>{attachment.value}</a>}
                    {!isRealUrl && <span>{attachment.value}</span>}
                    {!noDelete && <button type="button" onClick={e => this.removeAttachment(index)} className="delete is-small"></button>}
                </span>
            );
        } else {
            const url = server + "/file/" + attachment.value;
            return (
                <span className="tag" key={attachment.id || attachment.value}>
                    <a href={url}>{attachment.value.slice(36)}</a>
                    {!noDelete && <button type="button" onClick={e => this.removeAttachment(index)} className="delete is-small"></button>}
                </span>
            );
        }
    }

    setUser(e) {
        const currentNote = { ...this.state.selectedNote };
        if (currentNote.users.indexOf(e.target.value) === -1) {
            currentNote.users.push(e.target.value);
            this.setState({ selectedNote: currentNote });
        }
    }

    getUserName(id) {
        const user = this.state.users.find(u => u.id == id);
        if (user) {
            return `${user.firstName} ${user.lastName}`;
        }
    }

    removeUser(userId) {
        const currentNote = { ...this.state.selectedNote };
        currentNote.users.splice(currentNote.users.indexOf(userId), 1);
        this.setState({ selectedNote: currentNote });
    }

    removeAttachment(index) {
        const currentNote = { ...this.state.selectedNote };
        currentNote.attachments.splice(index, 1);
        this.setState({ selectedNote: currentNote });
    }

    markdownPreview(text) {
        return parse(marked(text));
    }

    render() {
        const courses = this.state.courses;
        const users = this.state.users;
        const sharedCourses = this.state.sharedCourses;
        const sharedNotes = this.state.sharedNotes;
        return (
            <div className="columns">
                <div className="menu column is-one-quarter">
                    <p className="menu-label">Courses</p>
                    <ul className="menu-list">
                        {courses.map(course => (
                            <li key={course.id}>
                                <a className={this.state.selectedCourse === course.id ? "is-active" : ""} onClick={e => this.selectCourse(course.id)}>
                                    {course.name}
                                </a>
                                <ul>
                                    {course.notes.map(note => (
                                        <li key={note.id}>
                                            <a
                                                className={this.state.selectedNote.id === note.id ? "is-active" : ""}
                                                onClick={e => this.selectNote(note.id, course.id)}
                                            >
                                                {note.name}
                                            </a>
                                        </li>
                                    ))}
                                </ul>
                            </li>
                        ))}
                    </ul>

                    <p className="menu-label">
                        Courses shared <small className="tag">{sharedCourses.length}</small>
                    </p>
                    <ul className="menu-list">
                        {sharedCourses.map(course => (
                            <li key={course.id}>
                                <a>{course.name}</a>
                                <ul>
                                    {course.notes.map(note => (
                                        <li key={note.id}>
                                            <a
                                                className={this.state.selectedNote.id === note.id ? "is-active" : ""}
                                                onClick={e => this.selectNote(note.id, course.id, true)}
                                            >
                                                {note.name}
                                            </a>
                                        </li>
                                    ))}
                                </ul>
                            </li>
                        ))}
                    </ul>
                    <p className="menu-label">
                        Notes shared <small className="tag">{sharedNotes.length}</small>
                    </p>
                    <ul className="menu-list">
                        {sharedNotes.map(note => (
                            <li key={note.id}>
                                <a
                                    className={this.state.selectedNote.id === note.id ? "is-active" : ""}
                                    onClick={e => this.selectNote(note.id, note.courseId, true)}
                                >
                                    {note.name}
                                </a>
                            </li>
                        ))}
                    </ul>
                </div>

                <div className="column">
                    <div className="container is-fluid">
                        {this.state.selectedNote.isShared && (
                            <div className="box">
                                <div className="columns">
                                    <div className="column">{this.state.selectedNote.name}</div>
                                    <div className="column">
                                        {this.state.selectedNote.id && (
                                            <button type="button" onClick={e => this.newNote()} className="button is-pulled-right is-primary is-outlined">
                                                New note
                                            </button>
                                        )}
                                    </div>
                                </div>

                                <hr />
                                <div className="content">{this.markdownPreview(this.state.selectedNote.content)}</div>
                                <hr />
                                <div className="tags">
                                    {this.state.selectedNote.attachments.map((attachment, index) => this.getAttachment(attachment, index, true))}
                                </div>
                            </div>
                        )}
                        {!this.state.selectedNote.isShared && (
                            <form action="" className="box" onSubmit={e => this.save(e)}>
                                <div className="columns">
                                    <div className="column">
                                        {!this.state.selectedCourse && <span className="tag">Please select a course in order to create a note</span>}
                                    </div>
                                    <div className="column"></div>
                                </div>
                                <div className="field">
                                    <p className="control">
                                        <label className="label">Name</label>
                                        <input
                                            value={this.state.selectedNote.name}
                                            onChange={e => this.handleChange("name", e)}
                                            className="input"
                                            type="name"
                                            required
                                            disabled={!this.state.selectedCourse}
                                            placeholder="Enter a name..."
                                        />
                                    </p>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <label className="label">Content</label>
                                        <div className="columns">
                                            <div className="column is-half-desktop">
                                                <textarea
                                                    value={this.state.selectedNote.content}
                                                    className="textarea"
                                                    disabled={!this.state.selectedCourse}
                                                    rows="10"
                                                    onChange={e => this.handleChange("content", e)}
                                                ></textarea>
                                            </div>
                                            <div className="column">
                                                <div className="content">{this.markdownPreview(this.state.selectedNote.content)}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <label className="label">Shared with</label>
                                        <div className="columns">
                                            <div className="column">
                                                {this.state.selectedNote.users.length === 0 && <small>No user selected for sharing</small>}
                                                <div className="tags">
                                                    {this.state.selectedNote.users.map(user => (
                                                        <span key={user} className="tag is-success">
                                                            {this.getUserName(user)}
                                                            <button
                                                                type="button"
                                                                onClick={e => this.removeUser(user)}
                                                                className="delete is-small"
                                                            ></button>
                                                        </span>
                                                    ))}
                                                </div>
                                            </div>
                                            <div className="column">
                                                <div className="select">
                                                    <select value={""} onChange={e => this.setUser(e)}>
                                                        <option>Select user for sharing</option>
                                                        {users.map(user => (
                                                            <option value={user.id} key={user.id}>
                                                                {user.firstName} {user.lastName} - {user.email}
                                                            </option>
                                                        ))}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <label className="label">Attachments</label>
                                        <div className="columns">
                                            <div className="column">
                                                <div className="tags">
                                                    {this.state.selectedNote.attachments.map((attachment, index) => this.getAttachment(attachment, index))}
                                                </div>
                                            </div>
                                            <div className="column">
                                                <div className="field has-addons">
                                                    <div className="control">
                                                        <input
                                                            value={this.state.attachment}
                                                            onChange={e => this.addAttachment(e)}
                                                            className="input"
                                                            type="text"
                                                            placeholder="Link to add"
                                                        />
                                                    </div>
                                                    <div className="control">
                                                        <a className="button is-info" onClick={e => this.saveAttachment()}>
                                                            Add
                                                        </a>
                                                    </div>
                                                </div>
                                                <div className="field">
                                                    <div className="control">
                                                        <input
                                                            type="file"
                                                            ref={this.fileInput}
                                                            onChange={e => this.onFileUpload(e)}
                                                            multiple
                                                            className="input"
                                                        />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <div className="buttons">
                                            <button className="button is-success">Save</button>
                                            <button
                                                type="button"
                                                disabled={!this.state.selectedNote.id}
                                                onClick={e => this.delete()}
                                                className="button is-danger"
                                            >
                                                Delete
                                            </button>
                                            {this.state.selectedNote.id && (
                                                <button type="button" onClick={e => this.newNote()} className="button is-primary is-outlined">
                                                    New note
                                                </button>
                                            )}
                                        </div>
                                    </div>
                                </div>
                            </form>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
