import React, { Component } from "react";
import Http from "../../http";
import { toast } from "bulma-toast";
class Groups extends Component {
    state = {
        groups: [],
        currentGroup: {
            id: "",
            name: "",
            users: [],
            courses: []
        },
        users: [],
        courses: []
    };

    handleChange(field, e) {
        const currentGroup = { ...this.state.currentGroup };
        currentGroup[field] = e.target.value;

        this.setState({ currentGroup });
    }

    handleArrayChange(field, e) {
        const currentGroup = { ...this.state.currentGroup };
        currentGroup[field] = [].slice.call(e.target.selectedOptions).map(o => {
            return o.value;
        });

        this.setState({ currentGroup });
    }

    edit(group) {
        const groupExt = { ...group };
        groupExt.users = group.users.map(el => el.id);
        groupExt.courses = group.courses.map(el => el.id);
        this.setState({ currentGroup: groupExt });
    }

    delete(id) {
        Http.delete(`/groups/${id}`).then(() => {
            this.loadGroups();
        });
    }

    save(e) {
        e.preventDefault();

        Http.post(
            "/groups" +
                (this.state.currentGroup.id
                    ? `/${this.state.currentGroup.id}`
                    : ""),
            this.state.currentGroup
        ).then(e => {
            this.loadGroups();
            toast({
                message: e.message,
                type: "is-success",
                position: "center"
            });
        });
    }

    loadGroups() {
        Http.get(`/groups`).then(data => {
            this.setState({ groups: data });
        });
    }
    componentDidMount() {
        this.loadGroups();
        Http.get("/users").then(data => {
            this.setState({ users: data });
        });
        Http.get("/courses").then(data => {
            this.setState({ courses: data });
        });
    }

    render() {
        const groups = this.state.groups;
        const users = this.state.users;
        const courses = this.state.courses;
        return (
            <div className="columns">
                <div className="column">
                    <div className="box">
                        {groups.length === 0 && <h3>No groups available</h3>}
                        {groups.map(group => (
                            <article className="media" key={group.id}>
                                <div className="media-content">
                                    <div className="content">
                                        <div className="columns">
                                            <div className="column is-narrow">
                                                <strong>{group.name}</strong>
                                            </div>
                                            <div className="column">
                                                <small>
                                                    <div className="tags">
                                                        {group.courses.map(
                                                            course => (
                                                                <span
                                                                    key={
                                                                        group.id +
                                                                        course.id
                                                                    }
                                                                    className="tag is-rounded"
                                                                >
                                                                    {
                                                                        course.name
                                                                    }
                                                                </span>
                                                            )
                                                        )}
                                                    </div>
                                                </small>
                                            </div>
                                        </div>

                                        <div className="tags">
                                            {group.users.map(user => (
                                                <div
                                                    key={group.id + user.id}
                                                    className="tag"
                                                >
                                                    {user.firstName}{" "}
                                                    {user.lastName}
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                    <nav className="level is-mobile">
                                        <div className="level-left">
                                            <button
                                                type="button"
                                                className="button is-small is-danger"
                                                onClick={e =>
                                                    this.delete(group.id)
                                                }
                                            >
                                                Delete
                                            </button>
                                            <button
                                                type="button"
                                                className="button is-small"
                                                onClick={e => this.edit(group)}
                                            >
                                                Edit
                                            </button>
                                        </div>
                                    </nav>
                                </div>
                            </article>
                        ))}
                    </div>
                </div>
                <div className="column">
                    <form
                        action=""
                        className="box"
                        onSubmit={e => this.save(e)}
                    >
                        <div className="field">
                            <p className="control">
                                <label>Name</label>
                                <input
                                    value={this.state.currentGroup.name}
                                    onChange={e => this.handleChange("name", e)}
                                    className="input"
                                    type="name"
                                    required
                                    placeholder="Enter a name..."
                                />
                            </p>
                        </div>
                        <div className="field">
                            <p className="control">
                                <label>Users</label>
                                <select
                                    multiple={true}
                                    value={this.state.currentGroup.users}
                                    onChange={e =>
                                        this.handleArrayChange("users", e)
                                    }
                                    className="input"
                                    type="name"
                                    placeholder="Users"
                                >
                                    {users.map(user => (
                                        <option value={user.id} key={user.id}>
                                            {user.firstName} {user.lastName} -{" "}
                                            {user.email}
                                        </option>
                                    ))}
                                </select>
                            </p>
                        </div>
                        <div className="field">
                            <p className="control">
                                <label>Courses</label>
                                <select
                                    multiple={true}
                                    value={this.state.currentGroup.courses}
                                    onChange={e =>
                                        this.handleArrayChange("courses", e)
                                    }
                                    className="input"
                                    type="name"
                                    placeholder="Courses"
                                >
                                    {courses.map(course => (
                                        <option
                                            value={course.id}
                                            key={course.id}
                                        >
                                            {course.name}
                                        </option>
                                    ))}
                                </select>
                            </p>
                        </div>

                        <div className="field">
                            <p className="control">
                                <button className="button is-success">
                                    Save
                                </button>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default Groups;
